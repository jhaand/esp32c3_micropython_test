Some small tests to get micropython going on the ESP32-C. 

I currently use a NodeMCU with an AI Thinker ESP2-C3-32S. 
It has 2 Megabytes of Flash according to esptool.py. 

The first problem comes from the serial port. You nee a terminal program that disables all 
flow control. 

See this [message](https://forum.micropython.org/viewtopic.php?f=18&t=10104&sid=29a3535b17769290caacfaa4b67237a1&start=10#p60480) on the micropython forum. 

I use picocom to achieve this. 
`picocom /dev/ttyUSB0 -b 115200 --lower-rts --lower-dtr `

This then shows the problem with the partition table. 

