#!/bin/sh
esptool.py --chip esp32-c3 --port /dev/ttyUSB0 erase_flash
#esptool.py --chip esp32-c3 --port /dev/ttyUSB0 --baud 460800 write_flash --flash_mode qio -z 0x1000 esp32c3-20210902-v1.17.bin
esptool.py --chip esp32-c3 --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size 2MB -z 0x10000 esp32c3-20210902-v1.17.bin
